import logging
import sys
import os
from logging.config import dictConfig
import allennlp

if os.environ.get("ALLENNLP_DEBUG"):
    LEVEL = logging.DEBUG
else:
    LEVEL = logging.INFO

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(name)s - %(message)s", level=LEVEL)

# logging_config = dict(
#    version=1,
#    formatters={
#        'f': {
#            'format':
#            '%(asctime)s: [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s'
#        }
#    },
#    handlers={
#        'h': {
#            'class': 'logging.StreamHandler',
#            'formatter': 'f',
#            'level': 'INFO',
#            'stream': 'ext://sys.stdout'
#        }
#    },
#    root={
#        'handlers': ['h'],
#        'level': 'INFO',
#    },
# )
#
# dictConfig(logging_config)
#
# logging.getLogger().addHandler(logging.NullHandler())
