import allennlp
import allennlp.data
from pathlib import Path
from typing import (Dict, Tuple, Optional, Union, Any, List, Type, TypeVar,
                    Iterable, Callable)
import entailment_datasets.utils as utils
import logging
import json
import pandas as pd
import tqdm
from .preprocessors import SnliPreprocessor, DropUnknownLabel
from allennlp.data.fields import Field, TextField, LabelField, MetadataField
from allennlp.data.instance import Instance
from allennlp.common.util import JsonDict
from allennlp.data.dataset_readers import DatasetReader
from allennlp.common.params import Params
from allennlp.data.token_indexers import SingleIdTokenIndexer, TokenIndexer
from allennlp.data.tokenizers import Tokenizer, WordTokenizer
import pickle

logger = logging.getLogger(__name__)

TNliReader = TypeVar("TNliReader", bound="SnliReader")
TSingleExamplePreprocessor = TypeVar(
    "TSingleExamplePreprocessor", bound="SingleExamplePreprocessor")


@DatasetReader.register('snli-reader2')
class SnliReader(allennlp.data.dataset_readers.snli.SnliReader):
    default_files: Dict[str, str] = dict(
        train='snli_1.0_train.jsonl',
        dev='snli_1.0_dev.jsonl',
        test='snli_1.0_test.jsonl')

    default_preprocessed_files: Dict[str, str] = dict(
        train='snli_1.0_train.jsonl',
        dev='snli_1.0_dev.jsonl',
        test='snli_1.0_test.jsonl')
    default_cache_dir = 'cache'

    url: str = 'http://nlp.stanford.edu/projects/snli/snli_1.0.zip'
    premise = 'sentence1'
    hypothesis = 'sentence2'
    label = 'gold_label'

    def __init__(self,
                 preprocessor: SnliPreprocessor,
                 datadir: str,
                 tokenizer: Tokenizer = None,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 lazy: bool = False):
        self.preprocessor = preprocessor
        super().__init__(
            tokenizer=tokenizer, token_indexers=token_indexers, lazy=False)
        self.cache_data(
            Path(datadir) / ('cache_' + self.preprocessor.__class__.__name__))

    @classmethod
    def _data_examples(cls, file_name: Path) -> Dict[str, Any]:
        with open(file_name, 'r') as data_file:
            logger.info("Reading file {}...".format(file_name))

            for line in tqdm.tqdm(data_file):
                example = json.loads(line)
                yield example

    def _read(
            self,
            data_file: Path,
    ) -> Iterable[allennlp.data.Instance]:
        examples = []
        logger.info('Reading examples from file {}'.format(data_file))
        logger.info('Preprocessing examples using {}'.format(
            self.preprocessor))

        for ex in self._data_examples(data_file):
            processed_ex = self.preprocessor(ex)

            if processed_ex is not None:
                examples.append(processed_ex)
        logger.info('Converting examples to Instances')
        instances = [
            self.example_to_instance(ex) for ex in tqdm.tqdm(examples)
        ]

        # write to cache
        # if _read is called that means there is no cache
        cache_file = self._get_cache_location_for_file_path(data_file)
        logger.info('Writing to cache at {}'.format(cache_file))
        # pd.DataFrame(examples).to_pickle(cache_file)
        with open(cache_file, 'wb') as f:
            pickle.dump(instances, f)

        return instances

    @classmethod
    def _single_split_reader(cls: Type[TNliReader],
                             cache_dir: Optional[Path] = None,
                             reader_args: Optional[Dict[str, Any]] = None):

        if reader_args is None:
            reader_args = {}
        reader = cls(**reader_args)

        if cache_dir is not None:
            reader.cache_data(str(cache_dir.absolute()))

        return reader

    @classmethod
    def _standard_splits_readers(
            cls: Type[TNliReader],
            data_root_dir: Path,
            parent_dir: Union[str, Path] = 'snli_1.0',
            cache_dir_relative_to_parent_dir: str = 'cache/default',
            clear_cache: bool = False,
            train: bool = True,
            dev: bool = True,
            test: bool = True,
            reader_params: Optional[Dict[str, Any]] = None,
    ):
        """Reads the standard splits of SNLI dataset.

        If the dataset is not present, it downloads it.

        Arguments:

            data_root_dir: Directory containing all datasets

            parent_dir: path to snli dataset directory relative to data_root_dir

            cache_dir:

            train:
        """
        # make sure that the parent has original data
        data_root_dir.mkdir(parents=True, exist_ok=True)
        parent_dir = data_root_dir / parent_dir

        if parent_dir.exists() and parent_dir.is_dir():
            pass
        else:
            # parent does not exist. Download data
            zip_filename = parent_dir.parent / Path(
                cls.url).name  # data_dir/dataset.zip
            utils.download_unzip(cls.url, zip_filename)

        for orig_file in list(cls.default_files.values()):
            if not (parent_dir / orig_file).exists():
                raise ValueError("Original data file {} does not exist".format(
                    parent_dir / orig_file))
        # create instances of self and assign cache
        cache_dir = parent_dir / cache_dir_relative_to_parent_dir
        logger.info("Using cache dir {}".format(cache_dir))

        if reader_params is None:
            reader_params = {}
        train_reader: Optional[TNliReader] = None
        standard_train_reader: Optional[
            Callable[[], Iterable[allennlp.data.Instance]]] = None

        if train:
            train_reader = cls._single_split_reader(cache_dir, reader_params)

            def standard_train_reader():
                return train_reader.read(
                    data_root_dir / parent_dir / cls.default_files['train'],
                    clear_cache=clear_cache)

        dev_reader: Optional[TNliReader] = None
        standard_dev_reader: Optional[Callable[[], Iterable[allennlp.data.
                                                            Instance]]] = None

        if dev:
            dev_reader = cls._single_split_reader(cache_dir, reader_params)

            def standard_dev_reader():
                return dev_reader.read(
                    data_root_dir / parent_dir / cls.default_files['dev'],
                    clear_cache=clear_cache)

        test_reader: Optional[TNliReader] = None
        standard_test_reader: Optional[Callable[[], Iterable[allennlp.data.
                                                             Instance]]] = None

        if test:
            test_reader = cls._single_split_reader(cache_dir, reader_params)

            def standard_test_reader():
                return test_reader.read(
                    data_root_dir / parent_dir / cls.default_files['test'],
                    clear_cache=clear_cache)

        return standard_train_reader, standard_dev_reader, standard_test_reader

    def _get_cache_location_for_file_path(self, file_path: str) -> str:
        return super()._get_cache_location_for_file_path(file_path) + '.pkl'

    def instance_to_dict(self, instance: Instance) -> Dict:
        res = {}

        for field_name, field_value in instance.fields.items():
            res[field_name] = field_value

        return res

    def instance_to_text(self, instance: Instance) -> str:
        return json.dumps(self.instance_to_dict(instance))

    def text_to_instance(
            self,  # type: ignore
            premise: str,
            hypothesis: str,
            label: Union[str, int] = None) -> Instance:
        # pylint: disable=arguments-differ
        fields: Dict[str, Field] = {}
        premise_tokens = self._tokenizer.tokenize(premise)
        hypothesis_tokens = self._tokenizer.tokenize(hypothesis)
        fields['premise'] = TextField(premise_tokens, self._token_indexers)
        fields['hypothesis'] = TextField(hypothesis_tokens,
                                         self._token_indexers)

        if label is not None:
            if isinstance(label, int):
                fields['label'] = LabelField(label, skip_indexing=True)
            else:
                fields['label'] = LabelField(label)

        metadata = {
            "premise_tokens": [x.text for x in premise_tokens],
            "hypothesis_tokens": [x.text for x in hypothesis_tokens],
            "label": label
        }
        fields["metadata"] = MetadataField(metadata)

        return Instance(fields)

    def _instances_from_cache_file(
            self, cache_file: str) -> Iterable[allennlp.data.Instance]:
        logger.info("Found and using cache {}".format(cache_file))
        #df = pd.read_pickle(cache_file)
        with open(cache_file, 'rb') as f:
            examples = pickle.load(f)

        return examples
        #logger.info("Converting examples to instances")

        # for i, row in tqdm.tqdm(df.iterrows(), total=df.shape[0]):
        #    dic = row.to_dict()
        #    yield self.text_to_instance(dic[self.premise],
        #                                dic[self.hypothesis], dic[self.label])

    def example_to_instance(self, example: Dict[str, Union[str, int]]
                            ) -> allennlp.data.Instance:
        label = example[self.label]
        premise = example[self.premise]
        hypothesis = example[self.hypothesis]

        return self.text_to_instance(premise, hypothesis, label)

    def JSON_to_instance(self, json_dict: JsonDict) -> allennlp.data.Instance:
        processed_json_dict = self.preprocessor(json_dict)
        label = processed_json_dict.get(self.label)
        premise = processed_json_dict[self.premise]
        hypothesis = processed_json_dict[self.hypothesis]

        return self.text_to_instance(premise, hypothesis, label)

    def read(self, file_path: str,
             clear_cache: bool = False) -> Iterable[allennlp.data.Instance]:
        """Need to override read allow for bulk loading from pkl
        file"""
        # look for binary cache

        if self._cache_directory:
            cache_file: Optional[Path] = Path(
                self._get_cache_location_for_file_path(file_path))

            if clear_cache and cache_file.exists() and cache_file.is_file():
                logger.info("Clearing cache in {}".format(cache_file))
                cache_file.unlink()
        else:
            cache_file = None
        instances: Optional[Iterable[allennlp.data.Instance]] = None

        if cache_file is not None:
            if cache_file.exists() and cache_file.is_file():
                instances = self._instances_from_cache_file(cache_file)
        # read from original file if no cache

        if instances is None:
            instances = self._read(file_path)  # _read will also write a cache

        if not isinstance(instances, list):
            instances = list(instances)

        return instances

    def serialize_instance(self, instance: allennlp.data.Instance):
        raise RuntimeError("Unsupported method")


class SciTailReader(SnliReader):
    default_files: Dict[str, str] = dict(
        train='snli_format/scitail_1.0_train.txt',
        dev='snli_format/scitail_1.0_dev.txt',
        test='snli_format/scitail_1.0_test.txt')

    default_preprocessed_files: Dict[str, str] = dict(
        train='snli_format/scitail_1.0_train.txt',
        dev='snli_format/scitail_1.0_dev.txt',
        test='snli_format/scitail_1.0_test.txt')
    default_cache_dir = 'cache'

    url: str = 'http://data.allenai.org.s3.amazonaws.com/downloads/SciTailV1.1.zip'
    premise = 'sentence1'
    hypothesis = 'sentence2'
    label = 'gold_label'

    @classmethod
    def _standard_splits_readers(
            cls: Type[TNliReader],
            data_root_dir: Path,
            parent_dir: Union[str, Path] = 'SciTailV1.1',
            cache_dir_relative_to_parent_dir: str = 'cache/default',
            clear_cache: bool = False,
            train: bool = True,
            dev: bool = True,
            test: bool = True,
            reader_params: Optional[Dict[str, Any]] = None,
    ):
        return super()._standard_splits_readers(
            data_root_dir, parent_dir, cache_dir_relative_to_parent_dir,
            clear_cache, train, dev, test, reader_params)
