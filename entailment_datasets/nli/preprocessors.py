from typing import (Dict, Tuple, Optional, Union, Any, List, Type, TypeVar,
                    Iterable, Callable)
from allennlp.common.registrable import Registrable
import logging
logger = logging.getLogger(__name__)


@Registrable.register('snli-preprocessor')
class SnliPreprocessor(Registrable):
    def __init__(self):
        super().__init__()

    def call(self, example: Any):
        return example

    def __call__(self, example: Any):
        return self.call(example)

    def __repr__(self):
        desc = ('This is a base class and is transparent -- does nothing')

        return str(self.__class__) + '\n' + desc

    def tag(self) -> str:
        return str(self.__class__)


@SnliPreprocessor.register('drop-unknown-label-preprocessor')
class DropUnknownLabel(SnliPreprocessor):
    def __init__(self, label: str = 'gold_label', unk: List[str] = ['-']):
        self.label = label
        self.unk = unk
        super().__init__()

    def call(self, example: Any):
        label_val = example.get(self.label)

        if label_val is not None:
            if label_val in self.unk:
                return None
            else:
                return example
        else:  # there is no label. return the example as is
            return example

    def __repr__(self):
        return "Drops examples with label value in {}".format(self.unk)


@SnliPreprocessor.register('int-label-preprocessor')
class IntLabels(DropUnknownLabel):
    """Drops unk labels, and converts labels to ints"""

    def __init__(self,
                 label: str = 'gold_label',
                 unk: List[str] = ['-'],
                 replace_values: Dict[str, Union[str, int]] = dict(
                     entailment = 1, neutral = 0, contradiction = 2)):
        super().__init__(label=label, unk=unk)
        self.replace_values = replace_values

    def call(self, example: Optional[Dict[str, str]]
             ) -> Optional[Dict[str, Union[str, int]]]:
        example = super().call(example)

        if example is not None:
            label_val = example.get(self.label)

            if label_val is None:  # no label, return as is
                return example
            replacement = self.replace_values.get(label_val)

            if replacement is not None:
                example[self.label] = replacement

        return example

    def __repr__(self):
        desc = super().__repr__()

        return desc + '\n Replace using:\n{}'.format(self.replace_values)


@SnliPreprocessor.register('entailment-vs-rest-preprocessor')
class EntailmentVsRest(DropUnknownLabel):
    def __init__(self,
                 label: str = 'gold_label',
                 unk: List[str] = ['-'],
                 replace_values: Optional[Dict[str, Union[str, int]]] = None
                 ) -> None:
        super().__init__(label, unk)

        if replace_values is None:
            self.replace_values: Dict[str, Union[str, int]] = {
                'contradiction': 'not_entailment',
                'neutral': 'not_entailment'
            }
        else:
            self.replace_values = replace_values

    def call(self, example: Optional[Dict[str, str]]
             ) -> Optional[Dict[str, Union[str, int]]]:
        example = super().call(example)

        if example is not None:
            label_val = example.get(self.label)

            if label_val is None:  # no label, return as is
                return example

            replacement = self.replace_values.get(label_val)

            if replacement is not None:
                example[self.label] = replacement

        return example

    def __repr__(self):
        desc = ('Does the following to each example:\n'
                '1. Replaces labels using replace_values dict. '
                'If nothing passed, converts contradiction and '
                'neutral to not_entailment\n'
                '2. Drops examples which have label == "-", ie. where '
                'annotators could not reach consensus')

        return str(self.__class__) + '\n' + desc

    def desc(self) -> str:
        s = str(self.__class__)

        if self.replace_values is not None:
            s += '_'.join([
                '_'.join([str(key), str(val)])

                for key, val in self.replace_values.items()
            ])

        return s


TSingleExamplePreprocessor = TypeVar(
    "TSingleExamplePreprocessor", bound="SingleExamplePreprocessor")
