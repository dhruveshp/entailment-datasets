from entailment_datasets.nli.dataset_readers import SciTailReader
from entailment_datasets.nli.preprocessors import DropUnknownLabel, IntLabels
from pathlib import Path
import logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s: [%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s',
    handlers=[logging.StreamHandler()])

logger = logging.getLogger(__name__)


def print_ex(examples, n):
    for i in range(n):
        logger.info("{}. ".format(i))
        logger.info(str(examples[i]))


def test_simple():
    preprocessor = IntLabels(
        label='gold_label',
        unk=['-'],
        replace_values=dict(entailment=1, neutral=0))
    reader_params = {'preprocessor': preprocessor}
    train, dev, test = SciTailReader._standard_splits_readers(
        data_root_dir=Path('temp_data').absolute(),
        cache_dir_relative_to_parent_dir=Path('cache') / 'test_scitail',
        train=True,
        dev=True,
        test=True,
        reader_params=reader_params)
    train_set = train()
    dev_set = dev()
    test_set = test()
    logger.info("Train examples")
    logger.info('---------------')
    print_ex(train_set, 5)
    logger.info("Dev examples")
    logger.info('---------------')
    print_ex(dev_set, 5)
    logger.info("Test examples")
    logger.info('---------------')
    print_ex(test_set, 5)
