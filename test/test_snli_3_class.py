from entailment_datasets.nli.dataset_readers import SnliReader
from entailment_datasets.nli.preprocessors import DropUnknownLabel, IntLabels
from allennlp.common.params import Params
from allennlp.training import util

from pathlib import Path
import logging
logger = logging.getLogger(__name__)


def print_ex(examples, n):
    for i in range(n):
        logger.info("1. ")
        logger.info(str(examples[i]))


def test_simple_3_class():
    preprocessor = DropUnknownLabel(label='gold_label', unk=['-'])
    reader_params = {'preprocessor': preprocessor}
    train, dev, test = SnliReader._standard_splits_readers(
        data_root_dir=Path('temp_data').absolute(),
        cache_dir_relative_to_parent_dir=Path('cache') / 'test_simple_3_class',
        train=True,
        dev=True,
        test=True,
        reader_params=reader_params)
    train_set = train()
    dev_set = dev()
    test_set = test()
    logger.info("Train examples")
    logger.info('---------------')
    print_ex(train_set, 5)
    logger.info("Dev examples")
    logger.info('---------------')
    print_ex(dev_set, 5)
    logger.info("Test examples")
    logger.info('---------------')
    print_ex(test_set, 5)


def test_simple_3_class_int_labels():
    preprocessor = IntLabels(
        label='gold_label',
        unk=['-'],
        replace_values={
            'entailment': 1,
            'neutral': 0,
            'contradiction': 2
        })
    reader_params = {'preprocessor': preprocessor}
    train, dev, test = SnliReader._standard_splits_readers(
        data_root_dir=Path('temp_data').absolute(),
        cache_dir_relative_to_parent_dir=Path('cache') / 'test_simple_3_class',
        clear_cache=True,
        train=True,
        dev=True,
        test=True,
        reader_params=reader_params)
    train_set = train()
    dev_set = dev()
    test_set = test()
    logger.info("Train examples")
    logger.info('---------------')
    print_ex(train_set, 5)
    logger.info("Dev examples")
    logger.info('---------------')
    print_ex(dev_set, 5)
    logger.info("Test examples")
    logger.info('---------------')
    print_ex(test_set, 5)


def test_simple_3_class_int_labels_from_params():
    params = Params({
        'dataset_reader': {
            'type': 'snli-reader2',
            'preprocessor': {
                'type': 'int-label-preprocessor'
            },
        },
        'train_data_path':
        '/Users/dhruv/UnsyncedDocuments/IESL/entailment/data/snli_1.0/snli_1.0_test.jsonl'
    })
    dataset = util.datasets_from_params(params.duplicate())
    train = dataset['train']
    logger.info("Train examples")
    logger.info('---------------')
    print_ex(train, 5)


if __name__ == '__main__':
    test_simple_3_class_int_labels_from_params()
