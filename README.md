Common library with common interface to read entailment datasets like SNLI, MNLI, etc.
Intended to be used as a library or submodule.